#!/usr/bin/env python3

# Standard modules
import datetime
import pathlib
# PyPI
from dateutil import tz
import suffix8601


TEST_FILES_DIR = pathlib.Path().cwd() / 'test_directory'


def delete_existing(verbose=False):
	global TEST_FILES_DIR
	for file_path in TEST_FILES_DIR.iterdir():
		file_path.unlink()
		if verbose:
			print('Deleted file: {}'.format(file_path))


def create_toml():
	toml = """[sample_group]
description="Just some ordinary TOML data"
sample_data="Hello World"
"""
	file_path = TEST_FILES_DIR / 'sample_toml_file.toml'
	file_conn = open(file_path, 'a')
	file_conn.write(toml)
	file_conn.close()
	print(f"Created file: '{file_path}'")


def create_test_files():
	""" Create sample files with varying datetimes and time zones"""
	base_filename = 'sample_backup.tar.gz'
	filecount = 0

	# 24 files for Los Angeles
	timezone_LAX = tz.gettz('America/Los Angeles')
	start_datetime = datetime.datetime(year=1955, month=11, day=5,
	                                   hour=15, minute=10, tzinfo=timezone_LAX)
	for i in range(0, 24, 2):  # 1 hour for 48 hours
		new_filename = suffix8601.StampFilename(TEST_FILES_DIR / base_filename,
		                                        start_datetime + datetime.timedelta(hours=i)).add_suffix()
		open(new_filename, 'a').close()
		filecount += 1

	# 24 files for New York
	timezone_NYC = tz.gettz('America/New York')
	start_datetime = datetime.datetime(year=1955, month=11, day=5,
	                                   hour=15, minute=10, tzinfo=timezone_NYC)
	for j in range(1, 25, 2):  # 1 hour for 48 hours
		new_filename = suffix8601.StampFilename(TEST_FILES_DIR / base_filename,
		                                        start_datetime + datetime.timedelta(hours=j)).add_suffix()
		open(new_filename, 'a').close()
		filecount += 1

	# 24 files for New York, another day
	timezone_NYC = tz.gettz('America/Chicago')
	start_datetime = datetime.datetime(year=1955, month=11, day=1,
	                                   hour=15, minute=10, tzinfo=timezone_NYC)
	for j in range(0, 72, 1):  # 1 hour for 48 hours
		new_filename = suffix8601.StampFilename(TEST_FILES_DIR / base_filename,
		                                        start_datetime + datetime.timedelta(hours=j)).add_suffix()
		open(new_filename, 'a').close()
		filecount += 1

	print(f'Created {filecount} new test files.')


if __name__ == "__main__":
	delete_existing()
	create_toml()
	create_test_files()
