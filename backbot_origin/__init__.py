# Standard modules
import logging
import pathlib
from operator import itemgetter
from os import environ as os_environ
# Custom modules
import suffix8601
import backbot_origin.config as config

# Global variables
# Setuptools reads this variable during package installation.
__version__ = "0.0.1"
__version_info__ = tuple(map(int, __version__.split('.')))

# Configure Python logging
logging.basicConfig(level=os_environ.get("LOGLEVEL", "INFO"))
logger = logging.getLogger(__name__)


# Instances methods
def getPackageVersion():
	return __version__


class RotateBackups():
	def __init__(self, path_to_config):
		self.config_path = path_to_config
		self.config = config.createConfigFromFile(self.config_path)

	def list_retained_files(self):
		"""Lists files that would be retained if rotate() was called."""
		for ruleset_name in self.config['rulesets']:
			ruleset = self.config['rulesets'][ruleset_name]
			file_list = []
			keep_files = set()
			print(f"\nProcessing ruleset: '{ruleset_name}' ({ruleset['description']})")
			for source_dir in ruleset['source_dirs']:
				source_path = pathlib.Path(source_dir)
				if not source_path.is_dir():
					raise Exception(f'The chosen directory {source_path} does not exist.')
				print(f'Parsing directory:  {source_path}')
				mycriteria = None
				if ruleset['file_extensions']:
					mycriteria = tuple(ruleset['file_extensions'])
				file_list += suffix8601.Directory(source_path).build_directory_metadata(mycriteria)['files']

			# Whether one directory or many, treat all files as a "set"
			file_list = suffix8601.add_sortcodes(file_list)
			# Process Rule:  keep_last_N
			if ruleset['keep_last_N'] > 0:
				quantity = ruleset['keep_last_N']
				result_list = [filedict for filedict in file_list if filedict['newness_in_list'] <= quantity]
				# sort the List
				for file_dict in sorted(result_list, key=itemgetter('newness_in_list')):
					logger.debug("{} : {} : {}".format(file_dict['newness_in_list'],
					                                   file_dict['utc_datetime'],
					                                   file_dict['full_name']))
					keep_files.add(file_dict['path'])
			if ruleset['keep_N_endofdays'] > 0:
				number_of_days = ruleset['keep_N_endofdays']
				result_list = [filedict for filedict in file_list if filedict['newness_in_day'] == 0 and filedict['date_occurence'] <= number_of_days]
				for file_dict in sorted(result_list, key=itemgetter('newness_in_list')):
					logger.debug("{} : {} : {}".format(file_dict['newness_in_list'],
					                                   file_dict['utc_datetime'],
					                                   file_dict['full_name']))
					keep_files.add(file_dict['path'])

		# Print Results
		for path in sorted(keep_files, reverse=True):
			print(path)
		print("Total files to keep: {}".format(len(keep_files)))

	def rotate(self):
		""" Rotates backups based on configuration criteria."""
