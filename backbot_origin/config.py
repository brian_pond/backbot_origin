# config.py
# Standard Library:
import pathlib
# PyPI:
import toml
from schema import Schema, And, Use
from schema import SchemaError, SchemaWrongKeyError, SchemaMissingKeyError
import sys

config_schema = Schema({  # Schema for TOML validation.
	'title': And(Use(str)),
	'rulesets': And(Use(dict)),
})


def createConfigFromFile(_filePath):
	'''Load generic bench configuration from bench_config.toml'''
	if not isinstance(_filePath, pathlib.Path.__base__):
		_filePath = pathlib.Path(_filePath)
	try:
		config_toml = toml.load(_filePath)
		if validate_toml_schema(config_schema, config_toml):
			return config_toml
		else:
			sys.exit(1)
	except Exception as e:
		if e.__class__.__name__ == 'FileNotFoundError':
			raise FileNotFoundError("Cannot find configuration file '{}'".format(_filePath))
		elif e.__class__.__name__ == 'TomlDecodeError':
			raise SyntaxError("There is a TOML syntax error in '{}'".format(_filePath))
		elif e.__class__.__name__ == 'SchemaError':
			raise SchemaError("There is a schema error in TOML file '{}'".format(_filePath))
		else:
			print(f"--> Unhandled Exception while loading '{_filePath}'")
			print(e)
			raise Exception('Error type is:', e.__class__.__name__)


def validate_toml_schema(toml_schema, toml_data):
	if not isinstance(toml_schema, Schema.__base__):
		raise Exception("Invalid schema definition.")
	try:
		toml_schema.validate(toml_data)
		return True
	except (SchemaError, SchemaWrongKeyError, SchemaMissingKeyError) as e:
		print("Schema error in '{}'".format('dbmigration_conf.toml'))
		print(e)
		return False  # by default, cleanly handle error.  Caller can always throw exception on False.
	except Exception as e:
		print("Unhandled Exception in function conf.validate_toml_schema()")
		print('Schema: {}'.format(toml_schema))
		print('Data: {}', format(toml_data))
		print('Exception type is:', e.__class__.__name__)
		raise Exception(e)
