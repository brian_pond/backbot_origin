# Backbot-Origin

This small application runs on servers that generate local backup files.

It examines those files, and deletes older ones, preventing the server from running out of disk space.

It's smart and focuses on quantity of backups; not timestamp.  If the *only* existing backup files are old?  It won't delete them.  Old backups are better than nothing.

## Installation
Recommend installing in a Python virtual environment.  This script will help you with that.
```
source make_venv.bash
```

Install using Python Setuptools and Pip:
```
pip install -e ./backbot_origin.repo/
```

Run by calling CLI with a parameter to a TOML configuration file.
```bash
python ../rotate_backups.py myconfig.toml
```

## Usage
