# example1.py
import backbot_origin
import argparse
import pathlib

if __name__ == "__main__":
	# Parse the 1st argument (path to a config file)
	parser = argparse.ArgumentParser()
	parser.add_argument("config_file", help="path to the TOML configuration file")
	args = parser.parse_args()
	if args.config_file:
		backbot_origin.RotateBackups(pathlib.Path(args.config_file)).list_retained_files()
