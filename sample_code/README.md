# How to use

## Example 1
From a terminal, run the following Python code:
```bash
python example1.py example_config.toml
```
This code lists files to keep:
1. The top 25 files, sorted by UTC datetime descending
2. The last file in a UTC day.  Keep the 10 most-recent days, if possible.

The files in sets 1 and 2 overlap.  So the actual result is not 35 files, but 28.
